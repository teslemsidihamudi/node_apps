const http = require('http');
const url = require('url');
const fs = require('fs');

http.createServer(function (req, res) {
    var query = url.parse(req.url,true).query;
    if (!query.name) {
        res.end('Don\'t be rude, what\'s your name?')
    } else {
        fs.appendFile('db.txt',  query.name  +'\n', (err) => {
            if (err) throw err;
            res.end('Hello ' + query.name)
            console.log('It\'s saved!');
        });
    }
}).listen(8956);
console.log("Server running at http://localhost:8956/");